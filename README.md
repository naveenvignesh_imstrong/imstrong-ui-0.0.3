### Imstrong styles v3

#### Gulp

```sh
$ gulp # builds styles and updates imstrong-server
```

#### Project Structure

- `_sass` - Contains the custom sass files for pages
- `_shared-sass` - Common sass to used across multiple sass files
- `css` - Generated Output
- `fonts` - All fonts files required for project
- `Image` - Image assets
- `rev-manifest.json` - Contains Css versioning.

#### Start Server

`npm run start-server`

Update port and other configuration at `server.js` file.

#### Compile and Watch Sass

```
sass --no-source-map --style=compressed --watch src.sass dest.css
```