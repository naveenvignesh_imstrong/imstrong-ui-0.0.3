const static_server = require("static-server");

const server = new static_server({
  rootPath: ".",
  port: 5000, // required, the port to listen
  name: "imstrong-ui-v3", // optional, will set "X-Powered-by" HTTP header
  cors: "*", // optional, defaults to undefined
  followSymlink: true // optional, defaults to a 404 error
});

server.start(function() {
  console.log("start server at port", server.port);
});
