"use strict";

// const path = require("path");
const fs = require("fs");
const gulp = require("gulp");
const sass = require("gulp-sass");
const watch = require("gulp-watch");
const rev = require("gulp-rev");
const del = require("del");
const replace = require("gulp-replace");

let zerodha_style_src = `${__dirname}/_sass/zerodha-landing.sass`;
let about_style_src = `${__dirname}/_sass/about.sass`;

let gen_folder = `${__dirname}/css/`;

let manifest_file = `${__dirname}/rev-manifest.json`;
let server_style_folder = `${__dirname}/../imstrong-server/public/static/v3/css/`;

function updateStyles(src) {
  gulp
    .src(src)
    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(gulp.dest(gen_folder))
    .pipe(rev())
    .pipe(gulp.dest(gen_folder))
    .pipe(
      rev.manifest(`${manifest_file}`, {
        base: gen_folder,
        merge: true
      })
    )
    .pipe(gulp.dest(gen_folder));
}

gulp.task("clean", function(done) {
  let manifest = JSON.parse(fs.readFileSync(manifest_file, "utf8"));

  const css_files = Object.values(manifest).map(f => `!${gen_folder}/${f}`)
  const css_files_server = Object.values(manifest).map(f => `!${server_style_folder}/${f}`);

  del.sync([`${gen_folder}/*.css`, ...css_files], {
    force: true
  });

  del.sync([`${server_style_folder}/*.css`, ...css_files_server], {
    force: true
  });

  done();
});

gulp.task("styles", function(done) {
  updateStyles(zerodha_style_src);
  updateStyles(about_style_src);
  done();
});

gulp.task("change_server_files", function(done) {
  let manifest = JSON.parse(fs.readFileSync(manifest_file, "utf8"));

  const css_files = Object.values(manifest).map(f => `${gen_folder}/${f}`)

  gulp
    .src(css_files)
    .pipe(gulp.dest(server_style_folder));

  gulp
    .src(`${__dirname}/../imstrong-server/views/zerodha.jade`)
    .pipe(replace(/zerodha-landing.+\.css/, manifest["zerodha-landing.css"]))
    .pipe(gulp.dest(`${__dirname}/../imstrong-server/views/`));

  gulp
    .src(`${__dirname}/../imstrong-server/views/about/about_layout.jade`)
    .pipe(replace(/about.+\.css/, manifest["about.css"]))
    .pipe(gulp.dest(`${__dirname}/../imstrong-server/views/about/`));

  done();
});

gulp.task("watch-styles", function() {
  watch(`${gen_folder}/**/*.sass`, function() {
    gulp.series("default");
  });
});

gulp.task("default", gulp.series("styles", "clean", "change_server_files"));
